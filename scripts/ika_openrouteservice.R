# <!-- coding: utf-8 -->
#
# la partie openrouteservice
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# source("geo/scripts/ika.R");openrouteservice_jour();
openrouteservice_jour <- function() {
  carp()
  openrouteservice_parcours_concat()
  openrouteservice_parcours_donnees()
}
#
# concaténation des parcours unitaires
# source("geo/scripts/ika.R");openrouteservice_parcours_concat();
openrouteservice_parcours_concat <- function() {
  carp()
  library(tidyverse)
  library(sf)
  ors_dir <- sprintf("%s/OpenRouteService", cfgDir)
  files <- list.files(ors_dir, pattern = "\\.geojson$", full.names = TRUE, ignore.case = TRUE)
  glimpse(files)
  for (i in 1:length(files) ) {
    carp("file: %s", files[i])
    dsn <- files[i]
    nc <- st_read(dsn)
# QGIS rajoute l'altitude
    nc <- st_zm(nc, drop=TRUE)
    dsn <- str_replace(dsn, '.*/', '')
    dsn <- str_replace(dsn, '\\..*$', '')
    nc$parcours <- dsn
    if( exists('parcours.sf') ) {
      parcours.sf <- rbind(parcours.sf, nc)
    } else {
      parcours.sf <- nc
    }
  }
  glimpse(parcours.sf)
  dsn <- sprintf('%s/openrouteservice.geojson', varDir)
  st_write(parcours.sf, dsn, delete_dsn=TRUE)
  carp("dsn: %s", dsn)
  return(invisible(parcours.sf))
}
# lecture des parcours unitaires
# source("geo/scripts/ika.R");openrouteservice_parcours_lire();
openrouteservice_parcours_lire <- function() {
  carp()
  library(sf)
  dsn <- sprintf('%s/openrouteservice.geojson', varDir)
  nc <- st_read(dsn, stringsAsFactors=FALSE)
  carp("dsn: %s", dsn)
  return(invisible(nc))
}
# source("geo/scripts/ika.R");openrouteservice_parcours_donnees()
openrouteservice_parcours_donnees <- function() {
  carp()
  library(tidyverse)
  library(dplyr)
  donnees.df <- donnees_parcours_lire() %>%
#    dplyr::select(-date1, -date2, -d, -id) %>%
    glimpse()
  nc <- openrouteservice_parcours_lire()
  nc <- st_transform(nc, 2154)
  nc$longueur <- as.integer(st_length(nc)/10)
  parcours.sf <- nc %>%
    st_set_geometry(NULL) %>%
    glimpse()
  df <- full_join(donnees.df, parcours.sf,  by=c('Parcours'='parcours')) %>%
    arrange(Parcours, Nom) %>%
    print(n=40) %>%
    glimpse()
  nc <- left_join(nc, donnees.df,  by=c('parcours'='Parcours')) %>%
    arrange(parcours, Nom) %>%
#    print(n=40) %>%
    glimpse()
  nc <- st_transform(nc, 4326)
  dsn <- sprintf('%s/parcours_ors.geojson', varDir)
  st_write(nc, dsn, delete_dsn=TRUE)
  carp("dsn: %s", dsn)
#  tex_df2table(df, num=TRUE, dossier="donnees")
}