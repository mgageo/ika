# <!-- coding: utf-8 -->
#
# la partie donnees
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d"Utilisation Commerciale - Partage des Conditions Initiales à l"Identique 2.0 France
#
#
# les points d"entrée suite à une modification du fichier de données
#
# source("geo/scripts/ika.R");donnees_parcours_jour();
donnees_parcours_jour <- function(pdf = TRUE) {
  carp()
  library(tidyverse)
#  library(lubridate)
  df <- donnees_excel_lire()
  donnees_troncons_especes(df)
#  return()
#  donnees_milieux_ecrire(df)
#  return()
  donnees_parcours_ecrire(df)
  donnees_troncons_ecrire(df)
  donnees_troncons_extrait()
  if (pdf == TRUE) {
    tex_pdflatex("donnees.tex")
  }
#  tex_pdflatex("troncons.tex")
  return()
  donnees_parcours_especes(df)
  donnees_parcours_couples(df)
  donnees_parcours_d5(df)
  donnees_parcours_traces()
}
#
# source("geo/scripts/ika.R");donnees_troncons_jour();
donnees_troncons_jour <- function(pdf = TRUE) {
  carp()
  library(tidyverse)
#  library(lubridate)
  df <- donnees_excel_lire()
#  donnees_troncons_especes(df)
  donnees_troncons_milieux_ecrire(df)
  donnees_troncons_ecrire(df)
  donnees_troncons_extrait()
#  stop("***")
#  tex_pdflatex("donnees.tex")
  if (pdf == TRUE) {
    tex_pdflatex("troncons.tex")
  }
}
#
# extraction des milieux des troncons
donnees_troncons_milieux_ecrire <- function(df) {
  carp()
  library(tidyverse)
  library(janitor)
  df1 <- df %>%
    glimpse() %>%
#    drop_na(DistanceKm) %>%
    group_by(Parcours, Troncon, MilieuDefinitif) %>%
    summarize(nb = n()) %>%
    ungroup() %>%
    print(n = 20)
#  stop("****")
  dsn <- sprintf("%s/donnees_troncons_milieux.Rds", cfgDir)
  saveRDS(df1, dsn)
}
donnees_troncons_milieux_lire <- function() {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/donnees_troncons_milieux.Rds", cfgDir)
  return(invisible(readRDS(dsn)))
}
#
# extraction des parcours
donnees_parcours_ecrire <- function(df) {
  carp()
  library(tidyverse)
  df1 <- df %>%
    mutate(ligne = 1:nrow(.)) %>%
    filter(is.na(DistanceParcours))
  if (nrow(df1) > 0) {
    View(df1)
    stop("***")
  }
  df1 <- df %>%
    drop_na(DistanceParcoursKm) %>%
    mutate(date1 = format(Date1, format = "%d/%m/%Y")
      , date2 = format(Date2, format = "%d/%m/%Y")
      , km = sprintf("%.2f", DistanceParcours)
    ) %>%
    mutate(d = as.numeric(Date2 - Date1, units = "days")) %>%
    dplyr::select(Nom, Parcours, Commune, date1, date2, d, km, DistanceParcours) %>%
    distinct(Nom, Parcours, Commune, date1, date2, d, km, DistanceParcours) %>%
    arrange(Parcours, Nom) %>%
    print(n = 40)
  tex_df2table(df1, num = TRUE, dossier = "donnees")
  dsn <- sprintf("%s/parcours.Rds", cfgDir)
  saveRDS(df1, dsn)
  return(invisible(df1))
}
#
# extraction des parcours
# source("geo/scripts/ika.R");donnees_parcours_extrait();
donnees_parcours_extrait <- function() {
  carp()
  library(tidyverse)
  library(gridExtra)
  library(janitor)
  df1 <- donnees_excel_lire() %>%
    group_by(Nom, Parcours, Troncon, DescriptionMilieu, MilieuDefinitif, Distance) %>%
    summarize(nb = n()) %>%
    glimpse()
  df2 <- df1 %>%
    group_by(Parcours) %>%
    summarize(nb = n())
  parcoursDir <- sprintf("%s/donnees_parcours", texDir)
  dir.create(parcoursDir, showWarnings = FALSE, recursive = TRUE)
  for (i in 1:nrow(df2)) {
    p <- df2[[i, "Parcours"]]
    carp("p: %s", p)
#    glimpse(p)
    df3 <- df1 %>%
      filter(Parcours == p) %>%
      dplyr::select(-Parcours)
    carp("p: %s nrow:%s", p, nrow(df3))
    tex_df2kable(df3, num = TRUE, suffixe = p, dossier = "donnees_parcours", extra = "donnees_parcours_extrait_kable")
  }
}
donnees_parcours_extrait_kable <- function(tex) {
  tex <- column_spec(tex, 4, width = "20em")
#  tex <- column_spec(tex, 5, width = "12em")
}
donnees_parcours_lire <- function() {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/parcours.Rds", cfgDir)
  return(invisible(readRDS(dsn)))
}
#
# l
# source("geo/scripts/ika.R");donnees_parcours_traces()
donnees_parcours_traces <- function() {
  carp()
  library(tidyverse)
  parcours.df <- donnees_parcours_lire()
  glimpse(parcours.df)
  parcours.sf <- fonds_parcours_lire_sf() %>%
    st_set_geometry(NULL)
  glimpse(parcours.sf)
  df <- full_join(parcours.df, parcours.sf, by = c("Parcours" = "parcours")) %>%
    select(-date1, -date2, -d, -id) %>%
    arrange(Nom, Parcours) %>%
    glimpse()
  tex_df2table(df, num = TRUE, dossier = "donnees")
}

#
# controle du nombre de couples
donnees_parcours_couples <- function(df) {
  carp()
  library(tidyverse)
  df1 <- df %>%
    dplyr::select(Troncon, Espece, Couple, Couple1, Couple2) %>%
    arrange(Troncon, Espece, Couple, Couple1, Couple2) %>%
    filter(Couple > pmax(Couple1, Couple2)) %>%
    print(n = 20)

  tex_df2table(df1, num = FALSE, dossier = "donnees")
}
#
# controle du nom des espèces
donnees_parcours_especes <- function(df) {
  carp()
  library(tidyverse)

  df1 <- df %>%
    group_by(Espece) %>%
    summarize(nb = n()) %>%
    arrange(Espece) %>%
    print(n = 20)
  tex_df2table(df1, num = TRUE, nb_lignes = 55, dossier = "donnees")
}
#
# les parcours avec moins de 5 données
donnees_parcours_d5 <- function(df) {
  carp()
  library(tidyverse)
  df$Couple <- as.integer(df$Couple)
  df1 <- df %>%
    mutate(lieudit = stringr::str_sub(Lieudit, 1, 30)) %>%
    group_by(Nom, Parcours, Commune, lieudit, Troncon) %>%
    summarize(nb_donnees = n(), nb_couples = sum(Couple)) %>%
    arrange(Nom, Parcours, Commune, lieudit, Troncon) %>%
    filter(nb_donnees < 5) %>%
    print(n = 20)
  tex_df2table(df1, num = FALSE, dossier = "donnees")
}
#
# les troncons
# =====================================================
#
# écriture du fichier
# source("geo/scripts/ika.R");df <- donnees_excel_lire();donnees_troncons_ecrire(df);
donnees_troncons_ecrire <- function(df) {
  carp()
  library(tidyverse)
  df1 <- df %>%
#    drop_na(DistanceKm) %>%
    group_by(Nom, annee, Commune, Parcours, Troncon, lieudit, Distance, DistanceParcours, MilieuDefinitif) %>%
    summarize(nb = n()) %>%
    arrange(Nom, annee, Commune, Parcours, Troncon) %>%
    ungroup() %>%
    print(n = 20)
#  stop("****")
  dsn <- sprintf("%s/troncons.Rds", cfgDir)
  saveRDS(df1, dsn)
}
#
# lecture des troncons
donnees_troncons_lire <- function() {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/troncons.Rds", cfgDir)
  return(invisible(readRDS(dsn)))
}
#
# extraction des troncons
# https://community.rstudio.com/t/looking-for-examples-displaying-data-with-multi-level-totals/1327/2 bind_rows
# source("geo/scripts/ika.R");donnees_troncons_extrait();
donnees_troncons_extrait <- function() {
  carp()
  library(tidyverse)
  library(knitr)
  library(kableExtra)
  library(gridExtra)
  library(janitor)
  df1 <- donnees_troncons_lire() %>%
    glimpse()
  df2 <- df1 %>%
    group_by(Parcours, Troncon) %>%
    summarize(nb = n()) %>%
    filter(nb > 1)
  if (nrow(df2) > 0) {
    carp("troncon avec caracteristiques differentes")
    df3 <- df1 %>%
      filter(Parcours %in% df2$Parcours) %>%
      filter(Troncon %in% df2$Troncon) %>%
    View(df2)
#    stop("*****")
  }
  df2 <- df1 %>%
    bind_rows(df1 %>%
      group_by(Nom, annee, Commune, Parcours, DistanceParcours) %>%
      summarize(
        Troncon = "total",
        Distance = sum(Distance)
      )
    ) %>%
    mutate(
      Tkm = sprintf("%.2f", Distance),
      Pkm = sprintf("%.2f", DistanceParcours)
    ) %>%
    dplyr::select(Nom, annee, Parcours, Troncon, lieudit, Tkm, Pkm) %>%
    arrange(Nom, annee, Parcours, Troncon) %>%
    glimpse()
  tex_df2table(df2, num = TRUE, dossier = "donnees")
  df3 <- df2 %>%
    filter(Troncon == "total") %>%
#    dplyr::select(Nom, annee, Parcours, Tkm, Pkm) %>%
    arrange(Parcours) %>%
    dplyr::select(-Troncon, -lieudit) %>%
    print(n = 40)
  tex_df2table(df3, num = TRUE, suffixe = "total", dossier = "donnees")
  df3 %>% get_dupes(Parcours) %>% print(n = 20)
#
# ajout des troncons
  troncons.sf <- fonds_troncons_lire_sf() %>%
    ungroup() %>%
    glimpse()
  l <- as.integer(st_length(troncons.sf))
  lg.df <- data.frame(longueur = l) %>%
    glimpse()
  troncons.df <- troncons.sf %>%
    st_drop_geometry() %>%
    dplyr::select(-longueur) %>%
    glimpse()
  troncons.df <- cbind(troncons.df, lg.df) %>%
    glimpse()
  df5 <- df1 %>%
    filter(Troncon != "total") %>%
    left_join(troncons.df, by = c("Parcours" = "parcours", "Troncon" = "troncon")) %>%
    mutate(lgx = as.numeric(Distance) * 1000, lgq = as.numeric(longueur)) %>%
    mutate(di = abs(lgx - lgq), mi = ifelse(lgx > lgq, lgq, lgx)) %>%
    mutate(ecart = round(di / mi, 0)) %>%
    glimpse()
#  View(df5);  stop("***")
  df6 <- df5 %>%
    filter(is.na(longueur)) %>%
    glimpse()
#  stop("****")
#  return()
  for (i in 1:nrow(df3)) {
    p <- as.character(df3[i, "Parcours"])
    carp("p: %s", p)
#    glimpse(p)
    df4 <- df5 %>%
      filter(Parcours == p) %>%
      dplyr::select(Troncon, lieudit, lgx, lgq, ecart)
    carp("p: %s nrow:%s", p, nrow(df4))
    tex_df2kable(df4, num = TRUE, suffixe = p, dossier = "parcours")
    next
# une version pdf
    parcoursDir <- sprintf("%s/parcours/%s", texDir, p)
    dir.create(parcoursDir, showWarnings = FALSE, recursive = TRUE)
    dsn <- sprintf("%s/parcours/parcours_%s_troncons.pdf", texDir, p, p)
    pdf(dsn, height = 8, width = 12)
    grid.table(df4)
    dev.off()
  }
}

#
# une ligne par tronçon avec les espèces
donnees_troncons_especes <- function(df) {
  carp()
  library(tidyverse)
  df1 <- df %>%
    mutate(troncon = sprintf("%s_%s", Parcours, Troncon)) %>%
    mutate(espece = sprintf("%s(%s,%s,%s)", Espece, Couple1, Couple2, Couple)) %>%
    dplyr::select(troncon, espece) %>%
    group_by(troncon) %>%
    summarize(especes = paste0(espece, collapse = ", ")) %>%
    print(n = 20)
  dsn <- sprintf("%s/donnees_troncons_especes.Rds", cfgDir)
  saveRDS(df1, dsn)
  carp("dsn: %s", dsn)
}
donnees_troncons_especes_lire <- function() {
  carp()
  library(tidyverse)

  dsn <- sprintf("%s/donnees_troncons_especes.Rds", cfgDir)
  df <- readRDS(dsn)
  carp("dsn: %s", dsn)
  return(invisible(df))
}
#
##  lecture du fichier de données Excel -----------------------------------
# =========================================================================
#
#
# lecture du fichier excel
# v3 : après reprise par Matthieu
# V4 : après vérif parcours le 10/12/2019 Marc/Matthieu
# V5 : ajout colonne  le 06/02/2020 Matthieu
# source("geo/scripts/ika.R"); df <- donnees_excel_lire()
donnees_excel_lire <- function(fic = "base_v6") {
  library(tidyverse)
  library(readxl)
  dsn <- sprintf("%s/%s.xlsx", varDir, fic)
  carp("dsn: %s", dsn)
  df <- read_excel(dsn)
  cols1 <- colnames(df)
  cols2 <- c("Espece", "Nom", "Parcours", "UneLettreParcours", "NordSud", "Commune", "Date1", "Date2", "Communes",
                    "Lieudit", "DescriptionMilieu", "GrandMilieu", "GrandMilieuMbe",
                    "MilieuVue", "MilieuStat", "MilieuDefinitif",
                    "Troncon", "MilieuHabitations", "TronconV1", "Couple1", "Couple2", "Couple",
                    "Remarque1", "Remarque2", "Distance", "DistanceKm", "DistanceParcours", "DistanceParcoursKm", "D0", "D25", "D100", "Remarque3")
  df1 <- data.frame(cols1, cols2)
#  View(df1)
  colnames(df) <- cols2
#  glimpse(df)
#  stop("***")
# colonnes pas nécessairement numériques, ex : ligne
  if (1 == 2) {
    df <- df %>%
      mutate(ligne = 1:nrow(.)) %>%
      replace_na(list(Couple = 0, Couple1 = 0, Couple2 = 0))
    df$Couple <- as.integer(df$Couple)
    df %>%
      filter(is.na(Couple)) %>%
      View(.)
    df$Couple1 <- as.integer(df$Couple1)
    df$Couple2 <- as.integer(df$Couple2)
  }
  df$lieudit <- strtrim(df$Lieudit, 50)
  df$annee <- format(df$Date1, format = "%Y")
  df$Parcours <- gsub("^Umb.*", "Umb", df$Parcours)
  glimpse(df)
  return(invisible(df))
}
donnees_excel_v7_lire <- function(fic = "base_v5") {
  library(tidyverse)
  library(readxl)
  dsn <- sprintf("%s/%s.xlsx", varDir, fic)
  carp("dsn: %s", dsn)
  df <- read_excel(dsn) %>%
    glimpse()
  cols1 <- colnames(df)
  cols2 <- c("Espece", "Nom", "Parcours", "UneLettreParcours", "NordSud", "Commune", "Date1", "Date2", "Communes",
                    "Lieudit", "DescriptionMilieu", "GrandMilieu", "GrandMilieuMbe",
                    "MilieuVue", "MilieuStat", "MilieuDefinitif",
                    "Troncon", "MilieuHabitations", "IntegrationHabitations", "TronconV1", "Couple1", "Couple2", "Couple",
                    "Remarque1", "Remarque2", "Distance", "DistanceKm", "DistanceParcours", "DistanceParcoursKm", "D0", "D25", "D100", "Remarque3")
  df1 <- data.frame(cols1, cols2)
#  View(df1)
  colnames(df) <- cols2
#  glimpse(df)
#  stop("***")
# colonnes pas nécessairement numériques, ex : ligne
  if (1 == 2) {
    df <- df %>%
      mutate(ligne = 1:nrow(.)) %>%
      replace_na(list(Couple = 0, Couple1 = 0, Couple2 = 0))
    df$Couple <- as.integer(df$Couple)
    df %>%
      filter(is.na(Couple)) %>%
      View(.)
    df$Couple1 <- as.integer(df$Couple1)
    df$Couple2 <- as.integer(df$Couple2)
  }
  df$lieudit <- strtrim(df$Lieudit, 50)
  df$annee <- format(df$Date1, format = "%Y")
  df$Parcours <- gsub("^Umb.*", "Umb", df$Parcours)
  glimpse(df)
  return(invisible(df))
}
#
# lecture du fichier excel
# v1
donnees_excel_v1_lire <- function() {
  carp()
  library(tidyverse)
  library(readxl)
  dsn <- sprintf("%s/base_v1.xlsx", varDir)
  df <- read_excel(dsn)
  glimpse(df)
  colnames(df) <- c("Espece", "Nom", "Parcours", "UneLettreParcours", "NordSud", "Commune", "Date1", "Date2", "Communes",
                    "Lieudit", "DescriptionMilieu", "GrandMilieu", "GrandMilieuMbe", "Troncon", "Couple1", "Couple2", "Couple",
                    "Remarque1", "Remarque2", "Distance", "DistanceKm", "DistanceParcours", "DistanceParcoursKm", "D0", "D25", "D100", "Remarque3")
  df$Couple <- as.integer(df$Couple)
  df$Couple1 <- as.integer(df$Couple1)
  df$Couple2 <- as.integer(df$Couple2)
  df$lieudit <- strtrim(df$Lieudit, 50)
  df$annee <- format(df$Date1, format = "%Y")
  glimpse(df)
  return(invisible(df))
}

#
# lecture du fichier excel
# v3 : après reprise par Matthieu
# V4 : après vérif parcours le 10/12/2019 Marc/Matthieu
# # source("geo/scripts/ika.R");donnees_excel_lire()
donnees_excel_lire_v4 <- function() {
  carp()
  library(tidyverse)
  library(readxl)
  dsn <- sprintf("%s/base_v4.xlsx", varDir)
  df <- read_excel(dsn)
#  glimpse(df)
  colnames(df) <- c("Espece", "Nom", "Parcours", "UneLettreParcours", "NordSud", "Commune", "Date1", "Date2", "Communes",
                    "Lieudit", "DescriptionMilieu", "GrandMilieu", "GrandMilieuMbe", "TronconV1", "Troncon", "Couple1", "Couple2", "Couple",
                    "Remarque1", "Remarque2", "Distance", "DistanceKm", "DistanceParcours", "DistanceParcoursKm", "D0", "D25", "D100", "Remarque3")
# colonnes pas nécessairement numériques, ex : ligne
  if (1 == 2) {
    df <- df %>%
      mutate(ligne = 1:nrow(.)) %>%
      replace_na(list(Couple = 0, Couple1 = 0, Couple2 = 0))
    df$Couple <- as.integer(df$Couple)
    df %>%
      filter(is.na(Couple)) %>%
      View(.)
    df$Couple1 <- as.integer(df$Couple1)
    df$Couple2 <- as.integer(df$Couple2)
  }
  df$lieudit <- strtrim(df$Lieudit, 50)
  df$annee <- format(df$Date1, format = "%Y")
  df$Parcours <- gsub("^Umb.*", "Umb", df$Parcours)
  glimpse(df)
  return(invisible(df))
}
#
## comparaison de deux versions du fichier ---------------------------------------------
#
# différence ma v6 et base de Matthieu
# source("geo/scripts/ika.R");donnees_excel_diff()
donnees_excel_diff <- function(fic1 = "base_v6", fic2 = "base_v8_mbe") {
  library(tidyverse)
  library(compareDF)
  df1 <- donnees_excel_lire(fic = fic1) %>%
#    dplyr::select(-`MilieuStat`, -`MilieuVue`, -`MilieuHabitations`, -`MilieuDefinitif`) %>%
#    dplyr::select(-UneLettreParcours, -GrandMilieuMbe) %>%
    mutate(Espece = tolower(Espece)) %>%
    mutate(MilieuDefinitif = tolower(MilieuDefinitif)) %>%
    mutate(Ligne = 1 : nrow(.)) %>%
    dplyr::select(Ligne, Espece, Troncon, Couple, MilieuDefinitif, MilieuHabitations, Commune) %>%
    glimpse()
#  stop("***")
#    dplyr::select(-`Couple1`, -`Couple2`, -`Couple`, -`D25`, -UneLettreParcours)
  df2 <- donnees_excel_lire(fic = fic2) %>%
#    dplyr::select(-UneLettreParcours, -GrandMilieuMbe)
#    dplyr::select(-`Couple1`, -`Couple2`, -`Couple`, -`D25`, -UneLettreParcours) %>%
    mutate(Espece = tolower(Espece)) %>%
    mutate(MilieuDefinitif = tolower(MilieuDefinitif)) %>%
    mutate(Ligne = 1 : nrow(.)) %>%
    dplyr::select(Ligne, Espece, Troncon, Couple, MilieuDefinitif, MilieuHabitations, Commune) %>%
  glimpse()
  champ <- "GrandMilieuMbe"
  champ <- "Espece"
  champ <- "Troncon"
  df3 <- df1 %>%
    group_by(champ = get(champ)) %>%
    summarize(nb = n())
  df4 <- df2 %>%
    group_by(champ = get(champ)) %>%
    summarize(nb = n())
  df5 <- df3 %>%
    left_join(df4, by = c("champ")) %>%
    glimpse() %>%
    filter(nb.x != nb.y) %>%
    print(n = 20)
#  stop("***")
  champs <- c("Ligne", "Espece", "Troncon", "Couple", "MilieuDefinitif", "MilieuHabitations")
  df3 <- compare_df(df1, df2, champs)
#  View(df3$comparison_df)
#  export_df2xlsx(df1)
  dsn <- sprintf("%s/%s_%s.xlsx", varDir, fic1, fic2)
  create_output_table(df3,
    output_type = "xlsx",
    file_name = dsn,
    limit = 20000,
    color_scheme = c(addition = "#52854C", removal = "#FC4E07", unchanged_cell = "#999999", unchanged_row = "#293352"),
    headers = NULL,
    change_col_name = "chng_type",
    group_col_name = "grp"
  )
  carp("dsn : %s", dsn)
}
#
# différence entre v5 et v4
donnees_excel_diff_v4 <- function() {
  library(tidyverse)
  library(compareDF)
  df1 <- donnees_excel_lire() %>%
    dplyr::select(-`MilieuStat`, -`MilieuVue`, -`MilieuHabitations`, -`MilieuDefinitif`) %>%
    dplyr::select(-UneLettreParcours, -GrandMilieuMbe)
  stop("***")
#    dplyr::select(-`Couple1`, -`Couple2`, -`Couple`, -`D25`, -UneLettreParcours)
  df2 <- donnees_excel_lire_v4() %>%
    dplyr::select(-UneLettreParcours, -GrandMilieuMbe)
#    dplyr::select(-`Couple1`, -`Couple2`, -`Couple`, -`D25`, -UneLettreParcours)
  champs <- c("Espece", "Nom", "Parcours", "Troncon", "Date1", "Date2")
#  all_equal(df1, df2, ignore_row_order = TRUE, ignore_col_order = TRUE)
  champ <- "GrandMilieuMbe"
  champ <- "Espece"
  champ <- "Troncon"
  df3 <- df1 %>%
    group_by(champ = get(champ)) %>%
    summarize(nb = n())
  df4 <- df2 %>%
    group_by(champ = get(champ)) %>%
    summarize(nb = n())
  df5 <- df3 %>%
    left_join(df4, by = c("champ")) %>%
    glimpse() %>%
    filter(nb.x != nb.y) %>%
    print(n = 20)
#  stop("***")

  df3 <- compare_df(df1, df2, champs)
#  View(df3$comparison_df)
#  export_df2xlsx(df1)
  dsn <- sprintf("%s/base_v5_v4.xlsx", varDir)
  create_output_table(df3,
    output_type = "xlsx",
    file_name = dsn,
    limit = 20000,
    color_scheme = c(addition = "#52854C", removal = "#FC4E07", unchanged_cell = "#999999", unchanged_row = "#293352"),
    headers = NULL,
    change_col_name = "chng_type",
    group_col_name = "grp"
  )
}
#
## validation de Parcours / Commune / Troncon
#
# source("geo/scripts/ika.R");donnees_commune_valide()
donnees_commune_valide <- function() {
  library(tidyverse)
  df1 <- donnees_excel_lire(fic = "base_v6") %>%
    mutate(Lettre = gsub("^([A-Z]+).*$", "\\1", Troncon)) %>%
    group_by(Parcours, Commune, Lettre) %>%
    summarize(nb = n()) %>%
    glimpse()
  export_df2xlsx(df1)
  carp("les parcours en double")
  df2 <- df1 %>%
    group_by(Parcours) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
  carp("les communes en double")
  df2 <- df1 %>%
    group_by(Commune) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
  carp("les lettres en double")
  df2 <- df1 %>%
    group_by(Lettre) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
}

