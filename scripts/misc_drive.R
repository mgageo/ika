# <!-- coding: utf-8 -->
# quelques fonctions pour les fichiers Google Drive
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d"Utilisation Commerciale - Partage des Conditions Initiales à l"Identique 2.0 France
#
# https://googledrive.tidyverse.org/
# https://cran.r-project.org/web/packages/googledrive/googledrive.pdf
# https://cran.r-project.org/web/packages/googlesheets/vignettes/managing-auth-tokens.html
# https://googledrive.tidyverse.org/articles/articles/multiple-files.html
#
#
# source("geo/scripts/ika.R"); drive_tex_get(path = "~/IKA/Participants", recursif = FALSE)
drive_tex_get <- function(path = "STOC", recursif = FALSE, test = 1) {
  carp()
  library(tidyverse)
  library(googledrive)
  drive_auth("univasso35@gmail.com")
#  url <- "https://drive.google.com/drive/u/0/folders/1wzEGfAy0Gk7aYdhDti9gRHG-nLgW7o4c"
  files <- drive_ls(path = path, pattern = ".tex", recursive = recursif) %>%
    glimpse()
  for(i in 1:nrow(files)) {
    file <- files[i, "name"]
    mga <<- files[i, "drive_resource"]
    carp("file: %s", file)
#    fichier <- gsub("^.*\\- ", "", file)
    fichier <- gsub(".tex$", ".pdf", file)
    carp("fichier: %s", fichier)
    dsn <- sprintf("%s/GoogleDrive/%s", cfgDir, fichier)
    drive_download(
      files[i, ],
      path = dsn,
      overwrite = TRUE
    )
  }
}