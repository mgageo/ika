# <!-- coding: utf-8 -->
#
# utilisation des données d'OpenStreetMap
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
# auteur : Marc Gauthier
#
#
#243500139;communauté d’agglomération de Rennes Métropole
#243500675;communauté de communes du Pays d’Aubigné
#243500659;communauté de communes du Pays de Châteaugiron
#243500774;communauté de communes du Pays de Liffré
#243500667;communauté de communes du Val d’Ille
#
osm_install <- function() {
  install.packages("Rcurl")
  install.packages("R.utils")
  install.packages("OpenStreetMap")
}
#
# la partie fonds de carte osm and co
# https://www.r-bloggers.com/the-openstreetmap-package-opens-up/
# https://github.com/mtennekes/tmap/tree/master/demo/LondonCrimes
# http://maps.stamen.com/terrain-background/#14/48.0868/-1.6917#
#
# carte dans la projection d'une grille
# plusieurs étapes :
# - détermination de la projection
# - détermination de l'emprise
# - agrandissement de l'emprise
# - calcul du polygone englobant
# - transformation de ce polygone en projection lonlat
# - extraction de la carte
# - transformation en format raster
# - découpe à l'emprise
# - affichage
osm_test <- function() {
  require(OpenStreetMap)
  require(raster)
  zone.spdf <- fonds_grille_lire()
  zone.crs <- proj4string(zone.spdf)
  bb <- bbox(zone.spdf)
  bb[,1] <- bb[,1] - 500
  bb[,2] <- bb[,2] + 500
  e <- extent(c(bb[1,1], bb[1,2], bb[2,1], bb[2,2]))
  clip.sp <- as(e, "SpatialPolygons")
  proj4string(clip.sp) <- CRS(zone.crs)
  lonlat <- spTransform(clip.sp, CRS("+init=epsg:4326"))
  boundingBox <- bbox(lonlat)
  Log(sprintf("upper left %s %s lower right %s %s", boundingBox[2,2], boundingBox[1,1], boundingBox[2,1], boundingBox[1,2]))
  nm <- c(
    "esri",
    "osm", "maptoolkit-topo", "bing",
    "stamen-toner", "stamen-watercolor", "stamen-terrain"
  )
  par(mfrow=c(3,3))
  for(i in 1:length(nm)){
    map <- openproj(openmap(c(boundingBox[2,2], boundingBox[1,1]), c(boundingBox[2,1], boundingBox[1,2]), 14, type=nm[i], minNumTiles=40), projection = zone.crs)
    plot(map)
  }
  stop("***")
  r <- raster(map)
  r <- crop(r, e)
  plotRGB(r)
  plot(zone.spdf, add=TRUE)
  rf <- writeRaster(r, filename="test.tif", format="GTiff", overwrite=TRUE)
}
#
# http://maps.stamen.com/m2i/#terrain-background/2000:1000/15/48.1026/-1.7450
osm_stamen <- function() {
  print(sprintf("osm_stamen()"))
  require(OpenStreetMap)
  require(raster)
  zone.spdf <- fonds_grille_lire()
  zone.crs <- proj4string(zone.spdf)
  bb <- bbox(zone.spdf)
  bb[,1] <- bb[,1] - 500
  bb[,2] <- bb[,2] + 500
  e <- extent(c(bb[1,1], bb[1,2], bb[2,1], bb[2,2]))
  clip.sp <- as(e, "SpatialPolygons")
  proj4string(clip.sp) <- CRS(zone.crs)
  lonlat <- spTransform(clip.sp, CRS("+init=epsg:4326"))
  boundingBox <- bbox(lonlat)
  Log(sprintf("upper left %s %s lower right %s %s", boundingBox[2,2], boundingBox[1,1], boundingBox[2,1], boundingBox[1,2]))
  url <- "http://a.tile.stamen.com/terrain-background/{z}/{x}/{y}.png"
  map <- openproj(openmap(c(boundingBox[2,2], boundingBox[1,1]), c(boundingBox[2,1], boundingBox[1,2]), 14, type=url, minNumTiles=40), projection = zone.crs)
  plot(map)
#  stop("***")
  r <- raster(map)
  r <- crop(r, e)
  plotRGB(r)
  plot(zone.spdf, add=TRUE)
  dsn <- sprintf("%s/stamen_terrain-background.tif", varDir)
  rf <- writeRaster(r, filename=dsn, format="GTiff", overwrite=TRUE)
}
osm_stamen_lire <- function() {
  require(raster)
  dsn <- sprintf("%s/stamen_terrain-background.tif", varDir)
  img <- brick(dsn)
  plotRGB(img)
}
#
# la patie api openstreetmap
# ======================================
#
# création d'une requête osm3
# un type et une emprise (boundingbox)
osm3_requete <- function(zone, type) {
  requete <- osm3_query(type)
  s="48.09";n="48.15";w="-1.38";e="-1.60"
# Acigné Servon
  w="-1.62";n="48.20";e="-1.30";s="48.00"
  body <- requete
  body <- gsub("\\$s", s, body)
  body <- gsub("\\$n", n, body)
  body <- gsub("\\$e", e, body)
  body <- gsub("\\$w", w, body)
  return(body)
}
#
# les différents types de requête disponible
osm3_query <- function(type) {
  switch(type,
# tous dans une bbox
  'bbox' = {
    body <- '<osm-script timeout="180">
<union>
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
  <recurse type="up"/>
</union>
<print/>
</osm-script>
'
  },
# limite des communes
  'limite_commune_rel' = {
    body <- '<osm-script timeout="180">
  <query type="relation">
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
    <has-kv k="admin_level" v="8"/>
    <has-kv k="boundary" v="administrative"/>
    <has-kv k="ref:INSEE"/>
   </query>
<print order="quadtile"/>
</osm-script>
'
  },
  'limite_commune_bbox' = {
#     <area-query ref="7465"/>
  body3 <- '<osm-script timeout="180">
 <union>
  <query type="relation">
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
    <has-kv k="admin_level" v="8"/>
    <has-kv k="boundary" v="administrative"/>
    <has-kv k="ref:INSEE"/>
   </query>
  <recurse type="relation-way"/>
  <recurse type="way-node"/>
</union>
<print order="quadtile"/>
</osm-script>
'
  },
# la limite d'une commune
  'limite_commune' = {
  body <- '<osm-script timeout="180">
<union>
  <query type="relation">
    <has-kv k="admin_level" v="8"/>
    <has-kv k="boundary" v="administrative"/>
    <has-kv k="ref:INSEE" v="35001"/>
  </query>
  <recurse type="relation-node" into="nodes"/>
  <recurse type="relation-way"/>
  <recurse type="way-node"/>
</union>
<print order="quadtile"/>
</osm-script>
'
  },
# limite de plusieurs communes
  'limite_communes' = {
    body <- '<osm-script timeout="180">
<union>
'
  aCommunes <- c("35001","35039","35051","35068", "35099","35207","35327")
  for (commune in aCommunes) {
    body <-sprintf('%s
  <query type="relation">
    <has-kv k="admin_level" v="8"/>
    <has-kv k="boundary" v="administrative"/>
    <has-kv k="ref:INSEE" v="%s"/>
  </query>', body, commune)
}
    body <-sprintf('%s
  <recurse type="relation-node" into="nodes"/>
  <recurse type="relation-way"/>
  <recurse type="way-node"/>
</union>
<print order="quadtile"/>
</osm-script>
', body)
  },
# limite d'un département
  'limite_departement' = {
# la relation du département
  body <- '<osm-script timeout="180">
  <query type="relation">
    <has-kv k="admin_level" v="6"/>
    <has-kv k="boundary" v="administrative"/>
    <has-kv k="name" v="Ille-et-Vilaine"/>
   </query>
<print order="quadtile"/>
</osm-script>
'
  },
# http://wiki.openstreetmap.org/wiki/FR:France_roads_tagging
  'highway' = {
  body <- '<osm-script timeout="180">
<union>
  <query type="way">
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
    <has-kv k="highway" v="motorway"/>
  </query>
  <query type="way">
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
    <has-kv k="highway" v="primary"/>
  </query>
  <query type="way">
    <bbox-query s="$s" n="$n" w="$w" e="$e"/>
    <has-kv k="highway" v="secondary"/>
  </query>
  <recurse type="way-node"/>
</union>
<print order="quadtile"/>
</osm-script>
'
  }
  )
  return(body)
}
#  http://www.omegahat.org/RCurl/philosophy.html
#
# utilisation de l'overpass avec requete en post
osm_xapi <- function() {
  library("RCurl")
  print(sprintf("osm_xapi() début"))
  h = basicTextGatherer()
  url <- 'http://overpass-api.de/api/interpreter'
  url <- 'http://api.openstreetmap.fr/oapi/interpreter'
  if ( url.exists(url) ) {
    print(sprintf("xapi() url.exists : %s", url))
    return;
  }
# limite des communes
  type <- 'highway'
  zone <- 'AcigneServon'
  query <- sprintf("%s_%s", type, zone)
  body <- osm3_requete(zone, type)
  print(sprintf("xapi() body: %s", body))
  f_osm3 <- sprintf("%s/osm/OSM/%s.osm3", baseDir, query)
  write(body, f_osm3)
  print(sprintf("xapi() f_osm3: %s", f_osm3))
  h$reset()
  curlPerform(url = url,
    httpheader=c(
      Accept="text/xml",
      Accept="multipart/*",
      'Content-Type' = "text/xml; charset=utf-8"
    ),
    postfields=body,
    writefunction = h$update,
    verbose = TRUE
  )
  response = h$value()
  f_osm <- sprintf("%s/osm/OSM/%s.osm", baseDir, query)
  write(response, f_osm)
  print(sprintf("osm_xapi() f_osm: %s", f_osm))
}
#
# les différents types de requête disponible
oapi_requete <- function(zone, type) {
  switch(zone,
  'Cesson' = {
    Ville <- 'cesson';
    ullr <-"-1.636687 48.160419 -1.550375 48.086584";
    code_insee <- '35051';
    osm_rel <- 78150
  }
  );
  switch(type,
  'highway' = {
    body <- sprintf('(area["ref:INSEE"="35051"]->.RM;way(area.RM)[highway][highway!=lane][highway!=track][highway!=service][access!=private];>;);out meta qt;', code_insee)
  },
  'bus_stop' = {
    body <- sprintf('area["ref:INSEE"="%s"]->.RM;node(area.RM)[highway=bus_stop];out meta qt;', code_insee)
  }
  )
  return(body)
}
# requête en get
osm_oapi <- function(data, f_osm) {
  library(httr)
  library(rvest)
  library(tidyverse)
  carp("début data:%s", data)
  url <- 'https://overpass-api.de/api/interpreter'
#  url <- 'http://api.openstreetmap.fr/oapi/interpreter'
#  url <- 'https://overpass-turbo.eu/'
  if ( url.exists(url) ) {
    print(sprintf("oapi() url.exists : %s", url))
    return;
  }
  res <- httr::RETRY ("POST", url, body = data)
  stop_for_status(res)
  doc <- httr::content (res, as = 'text', encoding="UTF-8", type = "application/xml") %>%
    glimpse()
  write(doc, f_osm)
  carp("f_osm: %s", f_osm)
}
# requête en get
osm_oapi_rcurl <- function(data, f_osm) {
  library("RCurl")
  carp("début data:%s", data)
  h = basicTextGatherer()
  url <- 'https://overpass-api.de/api/interpreter'
#  url <- 'http://api.openstreetmap.fr/oapi/interpreter'
#  url <- 'https://overpass-turbo.eu/'
  if ( url.exists(url) ) {
    print(sprintf("oapi() url.exists : %s", url))
    return;
  }
  url = sprintf('%s?data=[timeout:360][maxsize:1073741824];%s', url, URLencode(data));
  carp("url: %s", url)
  h = basicTextGatherer()
  h$reset()
  curlPerform(url = url,
    writefunction = h$update,
    verbose = FALSE
  )
  response = h$value()
  write(response, f_osm)
  carp("f_osm: %s", f_osm)
}
# requête en get
osm_oapi_json <- function(data, dsn) {
  library(httr)
  carp("début data:%s", data)
  url <- 'http://overpass-api.de/api/interpreter'
  data <- sprintf("[out:json][timeout:180];%s", data)
  url = sprintf('%s?data=%s', url, URLencode(data));
  carp("url: %s", url)
  res <- httr::GET(url)
  json <- httr::content(res, as="text", encoding="UTF-8")
  print(summary(json))
  write(json, dsn)
  print(sprintf("osm_oapi_json() dsn: %s",dsn))
}
# requête en get
osm_oapi_geojson <- function(data, dsn) {
  library(httr)
  carp("début data:%s", data)
  url <- 'http://overpass-api.de/api/interpreter'
  data <- sprintf("[out:GeoJson][timeout:180];%s", data)
  url = sprintf('%s?data=%s', url, URLencode(data));
  carp("url: %s", url)
  res <- httr::GET(url)
  json <- httr::content(res, as="text", encoding="UTF-8")
  print(summary(json))
  write(json, dsn)
  print(sprintf("osm_oapi_json() dsn: %s",dsn))
}
osm_oapi_bbox <- function(sf) {
  sf$cercle <- sf %>%
    st_buffer(500) %>%
    st_geometry()
  st_geometry(sf) <- "cercle"
  sf <- st_transform(sf, 4326)
  bb <- st_bbox(sf)
  opq0 <- opq(bbox = bb)
  opq1 <- add_osm_feature(opq = opq0, key = 'highway')
  print(opq_string(opq1))
  res1 <- osmdata_sf(opq1)
}
